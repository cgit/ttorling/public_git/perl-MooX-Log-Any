Name:           perl-MooX-Log-Any
Version:        0.004002
Release:        1%{?dist}
Summary:        A Moose role to add support for logging via Log::Any
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooX-Log-Any/
Source0:        http://www.cpan.org/authors/id/C/CA/CAZADOR/MooX-Log-Any-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make
BuildRequires:  findutils
BuildRequires:  coreutils
BuildRequires:  perl
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Run-time:
BuildRequires:  perl(Log::Any)
BuildRequires:  perl(Log::Any::Test)
BuildRequires:  perl(Moo)
BuildRequires:  perl(Moo::Role)
BuildRequires:  perl(Test::More)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
A logging role building a very lightweight wrapper to Log::Any for use with
your Moo or Moose classes. Connecting a Log::Any::Adapter should be
performed prior to logging the first log message, otherwise nothing will
happen, just like with Log::Any

%prep
%setup -q -n MooX-Log-Any-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{!?_licensedir:%global license %%doc}
%license LICENSE
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Tue Jul 21 2015 Tim Orling <ticotimo@gmail.com> - 0.004002-1
- Update to 0.004002
- Cleanup spec file per review (#1242726)

* Thu Apr 02 2015 Tim Orling <ticotimo@gmail.com> - 0.004001-1
- Specfile autogenerated by cpanspec 1.78.
